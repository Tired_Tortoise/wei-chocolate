<?php
/**
 * Created by PhpStorm.
 * User: mace1
 * Date: 6/29/15
 * Time: 8:26 AM
 */

session_start();
//assign all posted values to a session
if (!empty($_POST)) {
    foreach($_POST as $key => $value) {
        $_SESSION['your_form'][$key] = $value;
    }
}

require_once("myWrapper.php");


// if we are coming back from auth get the request code from the URL
if(isset($_GET['code']))
{
    $infusionsoftApiWrapper = new InfusionsoftApiWrapper('http://dev.tiredtortoise.com/samples/checkout.php', $_GET['code']);

}else{
    $infusionsoftApiWrapper = new InfusionsoftApiWrapper('http://dev.tiredtortoise.com/samples/checkout.php');
}

//Check if contact and has specific tag
$query =    array(
    'Email' => $_POST['Email'],
    'Groups' => 3433
);
$returnFields =    array('Id','FirstName', 'LastName');

$conInfo = $infusionsoftApiWrapper->call('data', 'query', array('Contact', 10, 0, $query, $returnFields, 'Id', false));


if(isset($conInfo[0])){
    //return to page and notify they need to use a different email
    header( 'Location: http://dev.tiredtortoise.com/orderForm.php?error=1' ) ;
}else{
    //Check if contact exists at all
    $query =    array(
        'Email' => $_POST['Email'],
        'FirstName' => $_POST['FirstName']
    );
    $returnFields =    array('Id','FirstName', 'LastName');

    $conInfo = $infusionsoftApiWrapper->call('data', 'query', array('Contact', 10, 0, $query, $returnFields, 'Id', false));

    if(isset($conInfo[0])){
        //don't creatw contact so we don't get duplicates
        $userId = $conInfo[0]['Id'];

    }else{
        //Add Contact if one does not exist
        $data = array('FirstName' => $_POST['FirstName'],
            'LastName' => $_POST['LastName'],
            'Email' => $_POST['Email']
        );

        $userId = $infusionsoftApiWrapper->call('data', 'add', array('Contact', $data));
    }

    //Validate Credit Card
    $valid = $infusionsoftApiWrapper->call('invoices', 'validateCreditCard', array($_POST['CCType'], $userId, $_POST['CCNumber'], $_POST['CCMonth'], $_POST['CCYear'], $_POST['CCSecurity']));

    if($valid['Valid'] == 'true'){
    //card is valid
        //add the rest of the data
        $conData = array(
            'Address2Street1' => $_POST['Add1'],
            'Address2Street2' => $_POST['Add2'],
            'City2' => $_POST['City'],
            'State2' => $_POST['State'],
            'PostalCode2' => $_POST['Zip'],
            'Phone1' => $_POST['Phone'],
            '_SubscriptionFlavor1' => $_POST['SubsFlavor1'],
            '_SubscriptionFlavor2' => $_POST['SubsFlavor2'],
            '_SubscriptionFlavor3' => $_POST['SubsFlavor3'],
            '_GiftGiverforgiftsubscriptions' => $_POST['BillFirstName'] . ' ' . $_POST['BillLastName']

        );

        $infusionsoftApiWrapper->call('contacts', 'update', array($userId, $conData));

        //create array for credit card send to inf for payment
        $card = array(
            'FirstName' => $_POST['BillFirstName'],
            'LastName' => $_POST['BillLastName'],
            'BillAddress1' => $_POST['BillAdd1'],
            'BillAddress2' => $_POST['BillAdd2'],
            'BillCity' => $_POST['BillCity'],
            'BillState' => $_POST['BillState'],
            'BillZip' => $_POST['BillZip'],
            'PhoneNumber' => $_POST['BillPhone'],
            'Email' => $_POST['BillEmail'],
            'CardType' => $_POST['CCType'],
            'CardNumber' => $_POST['CCNumber'],
            'ExpirationMonth' => $_POST['CCMonth'],
            'ExpirationYear' => $_POST['CCYear'],
            'CVV2' => $_POST['CCSecurity'],
            'ContactId' => $userId,

        );

        //add credit card
        $cardId = $infusionsoftApiWrapper->call('data', 'add', array('CreditCard', $card));


        //product set price
        if($_POST['subType'] == '232'){
            $price = 35;
        }elseif($_POST['subType'] == '234'){
            $price = 59;
        }elseif($_POST['subType'] == '228'){
            $price = 89;
        }



        //add order item
        $subId = $infusionsoftApiWrapper->call('invoices', 'addRecurringOrder', array((int)$userId, false, '151', 1, (double)$price, false, 10, (int)$cardId, 0, 0));

        //Create a Subscription Invoice
        $invoiceId = $infusionsoftApiWrapper->call('invoices', 'createInvoiceForRecurring', array((int)$subId));

        //Pay Invoice
        $response = $infusionsoftApiWrapper->call('invoices', 'chargeInvoice', array((int)$invoiceId, 'Gift Subscription Charge', (int)$cardId, 10, false));

        $data = array('contactIDNumber' => $userId,
            'tagIDNumber' => '3433'
        );

        $userId = $infusionsoftApiWrapper->call('contacts', 'addToGroup', $data);

        header( 'Location: http://weiofchocolate.com/pages/thank-you' ) ;

    }else{
//card is not valid return to form and notify
        header( 'Location: http://dev.tiredtortoise.com/orderForm.php?error=2' );
    }

}



