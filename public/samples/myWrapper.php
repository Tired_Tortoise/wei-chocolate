<?php
/**
 * Created by PhpStorm.
 * User: mace1
 * Date: 6/17/15
 * Time: 9:10 AM
 */


require_once "../vendor/autoload.php"; // path to the composer autoloader


class InfusionsoftApiWrapper
{

    //all of our important variables
    private $clientId = '62ehzjgt7zqdkzm2dnprvfrx';
    private $clientSecret = 'fG5kgXeQeh';
    private $redirectURL = '';
    private $token;
    private $code;
    private $infusionsoft;

    private $debug = false; // always set to false for now :S

    function __construct($URL, $codeForRefresh="")
    {
        //save the code for refreshing the token
        $this->code = $codeForRefresh;

        if(!file_exists("refreshToken.php")){
            $last_id_file = fopen("refreshToken.php", "w");
            fwrite($last_id_file, '<?php $refreshToken = "";');
            fclose($last_id_file);
        }

        //get the refresh token and save it as a variable the class can use
        include_once("refreshToken.php");

        $this->token = $refreshToken;

        $this->redirectURL = $URL;

    }


    // shows errors
    public function showDebug()
    {
        var_dump($this->infusionsoft->getLogs());
    }


    public function infuDate()
    {
        $date = $this->infusionsoft->formatDate();

        return $date;
    }

    /**
     * Main call wrapper for the Infusionsoft API
     * @param String $method
     * @param String $call | infusionsoft SDK function name
     * @param Array $args | Array of function params
     * @return Mixed
     */
    public function call($method, $call, $args=false, $count=0)
    {
        if($count > 10)
        {
            echo '10 failed tries made to: Infusionsoft->'.$method.'->'.$call.'() passing the arguments:<br/>';
            var_dump($args);
            exit;
        }

        $this->infusionsoft = new \Infusionsoft\Infusionsoft(array(
            'clientId'     => $this->clientId,
            'clientSecret' => $this->clientSecret,
            'redirectUri'  => $this->redirectURL
        ));

    /*
    * Connect to API via OAuth2
    */

    // check for token in session
    if(!isset($_SESSION['token']))
    {
        // no? Get last stored token out of .php file
        $dbToken = $this->token;

        // check if first time and force auth
        if($dbToken == "")
        {
            // If we are returning from Infusionsoft we need to exchange the code for an access token.
            if ($this->code != "" and !$this->infusionsoft->getToken()) {
                $this->infusionsoft->requestAccessToken($this->code);

                // Store this in DB & session
                $_SESSION['token'] = serialize($this->infusionsoft->getToken()); // for later
                $last_id_file = fopen("refreshToken.php", "w");
                fwrite($last_id_file, '<?php $refreshToken = \'' . serialize($this->infusionsoft->getToken()) . '\';');
                fclose($last_id_file);
            }
            else // should only ever need to do this once, works with localhost and local domain names (eg: yourwebsite.dev)
            {
            echo '<a href="' . $this->infusionsoft->getAuthorizationUrl() . '">Authorize with Infusionsoft</a>';
            exit;
            }
        }
        else
        {
        $_SESSION['token'] = $dbToken;
        }
    }

    // load old token into infusionsoft object
    $this->infusionsoft->setToken( unserialize($_SESSION['token']) );

    // try API call
    try {
        $data = call_user_func_array(array($this->infusionsoft->{$method}, $call), $args);
    } catch (\Infusionsoft\TokenExpiredException $ex) {

        // refresh token if invalid
        $this->infusionsoft->refreshAccessToken();


        // update DB with new token
        $last_id_file = fopen("refreshToken.php", "w");
        fwrite($last_id_file, '<?php $refreshToken = \'' . serialize($this->infusionsoft->getToken()) . '\';');
        fclose($last_id_file);

        // update session with new token
        $_SESSION['token'] = serialize($this->infusionsoft->getToken());

        // try again
        $data = $this->call($method, $call, $args, $count++);
    }

        // debug
        // var_dump($data); exit;

    return $data;
    }


}