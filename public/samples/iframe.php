<html>
<body>

<!-- jQuery -->
<script src="../assets/jquery/dist/jquery.min.js"></script>

<script>
    function listener(event){
        if ( event.origin !== "http://javascript.info" )
            $.ajax({
                type: "POST",
                data: event.data,
                url: "http://dev.tiredtortoise.com/samples/checkemail.php",
                success: function(data) {
                    if(data == "Hello"){
                        //document.getElementById("test").innerHTML = "repeat: "+event.data
                        window.parent.postMessage("repeat","http://dev.tiredtortoise.com");
                    }else{
                        //document.getElementById("test").innerHTML = "good: "+event.data
                        window.parent.postMessage("good","http://dev.tiredtortoise.com");
                    }
                }
            });

    }

    if (window.addEventListener){
        addEventListener("message", listener, false)
    } else {
        attachEvent("onmessage", listener)
    }
</script>


</body>
</html>

