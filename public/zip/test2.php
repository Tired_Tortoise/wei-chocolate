<?php

session_start();

require_once '../vendor/autoload.php';

$infusionsoft = new \Infusionsoft\Infusionsoft(array(
    'clientId' => '62ehzjgt7zqdkzm2dnprvfrx',
    'clientSecret' => 'fG5kgXeQeh',
    'redirectUri' => 'http://dev.successengine.net/test/newiSDK/samples/test2.php',
));

// check for token in session
if(!isset($_SESSION['token']))
{
    // no? Get last stored token out of database
    //open file to pull refresh token

    $last_id_file = fopen("lastId.txt", "r") or die("Unable to open file!");
    if (is_readable("lastId.txt")) {
        $dbToken = fread($last_id_file,filesize("lastId.txt"));
    }else{
        $dbToken = "";
    }
    fclose($last_id_file);

    // check if first time and force auth
    if($dbToken == '')
    {
        // If we are returning from Infusionsoft we need to exchange the code for an access token.
        if (isset($_GET['code']) and !$infusionsoft->getToken()) {
            $infusionsoft->requestAccessToken($_GET['code']);


            // Store this in DB & session
            $_SESSION['token'] = serialize($infusionsoft->getToken()); // for later
            $last_id_file = fopen("lastId.txt", "w");
            fwrite($last_id_file, serialize($infusionsoft->getToken()));
            fclose($last_id_file);
        }else{
            echo '<a href="' . $infusionsoft->getAuthorizationUrl() . '">Authorize with Infusionsoft</a>';
            exit;
        }
    }else{
        $_SESSION['token'] = $dbToken;
    }
}

// load old token into infusionsoft object
$infusionsoft->setToken( unserialize($_SESSION['token']) );

function addWithDupCheck($infusionsoft) {
    $contact = array('FirstName' => 'John', 'LastName' => 'Doe', 'Email' => 'johndoe@mailinator.com');

    return $infusionsoft->contacts->addWithDupCheck($contact, 'Email');
}

try {
    $cid = addWithDupCheck($infusionsoft);
} catch (\Infusionsoft\TokenExpiredException $e) {
    // If the request fails due to an expired access token, we can refresh
    // the token and then do the request again.
    $infusionsoft->refreshAccessToken();
    // Store this in DB & session
    $_SESSION['token'] = serialize($infusionsoft->getToken()); // for later
    $last_id_file = fopen("lastId.txt", "w");
    fwrite($last_id_file, serialize($infusionsoft->getToken()));
    fclose($last_id_file);

    $cid = addWithDupCheck($infusionsoft);
}

$contact = $infusionsoft->contacts->load($cid, array('Id', 'FirstName', 'LastName', 'Email'));

var_dump($contact);
