<?php

session_start();

if(isset($_SESSION['your_form'])){
        $form_data = $_SESSION['your_form'];
    }

?>

<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="assets/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="assets/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="assets/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        body{
            background-color: #ffffff;
        }
        .panel-heading {
            background: rgba(0, 0, 0, 0) -moz-linear-gradient(center top, #fefcf9, #ecdfc9) repeat scroll 0 0;
        }

        img{
            max-width: 80%;
            max-height: auto;
            margin-left: 10%;
            padding-bottom: 5px;
        }
        h3{
            margin-top: 5px;
            margin-bottom: 5px;;
        }
        h4{
            font-size: 1.5vw;
        }
        .updateCart{
            font-size: 10px;
            margin-left: 30%;
        }
        .placeOrder{
            background: rgba(0, 0, 0, 0) -moz-linear-gradient(center top , #985e30, #62210b) repeat scroll 0 0;
            border: 1px solid #62210b;
            border-radius: 4px;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            outline: medium none;
            text-align: center;
            text-decoration: none;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
            max-width: 125px;
            float: right;
        }
        .placeOrder:hover {
            color: #ffffff;
            background: rgba(0, 0, 0, 0) -moz-linear-gradient(center top , #985e30, #3F1709) repeat scroll 0 0;
        }
        input.error{
            border: 1px solid red;
        }
        input.error:hover{
            border: 1px solid red;
        }
        input.error:focus{
            border: 1px solid red;
        }
        label.error{
            color: red;
            font-size: 12px;
            padding-top: 5px;
        }

        @media only screen and (max-width: 767px) {
            h3{
                font-size: 15px;
            }
            h4 {
                font-size: 12px;
            }
            h5{
                font-size: 10px;
            }
        }
    </style>
</head>
<body>
<div id="wrapper">
    <div class="row">
        <form id="orderForm" action="samples/checkout.php" method="post">
            <div class="col-lg-1 col-sm-1 col-xs-1"></div>
            <div class="col-lg-10 col-sm-10 col-xs-10">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <img src="https://d1yoaun8syyxxt.cloudfront.net/owt88434-d62b0ed6-f57b-4278-a2db-ae9d9bcdf39c-v2">
                        <?php
                        if(isset($_GET['error'])){
                            ?>
                            <div class="row" >
                                <?php if($_GET['error'] == '1'){ ?>
                                    <div class="alert alert-danger">
                                        Oops! Your email address is already connected to an account with us, and in order to make this work smoothly, we need to link this to a separate email. If you have another email you can use, please enter it now. If not, call us at 602-577-2338 and we’ll help you out.
                                    </div>
                                <?php }elseif($_GET['error'] == '2'){ ?>
                                    <div class="alert alert-danger">
                                        Oops! Looks like there was a problem with your Credit Card. Please try again and if the problem persist please call us at 602-577-2338 and we’ll help you out.
                                    </div>
                                <?php } ?>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-lg-11 col-sm-11 col-xs-10">
                                        <h3>Products</h3>
                                    </div>
                                    <div class="col-lg-1 col-sm-1 col-xs-2">
                                        <h4>Total</h4>
                                    </div>
                                </div>

                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-2 col-sm-2">
                                        <img id="pic1" src="https://d1yoaun8syyxxt.cloudfront.net/owt88434-f8f9a500-09ab-443a-a663-c059047828d5-v2">
                                        <img style="display: none;" id="pic2" src="https://d1yoaun8syyxxt.cloudfront.net/owt88434-e9700985-1abe-4cd7-b331-3d84ce95d40d-v2">
                                        <img style="display: none;" id="pic3" src="https://d1yoaun8syyxxt.cloudfront.net/owt88434-60eb1dce-39ff-45e7-93ff-5fe105199eda-v2">
                                    </div>
                                    <div class="col-lg-7 col-sm-7 col-xs-10">
                                        <h4>Gift Subscription</h4>
                                        <p>The chocolate subscription will be shipped to your gift recipient at approximately the same time every month, allowing for postal holidays and weather considerations. Your credit card on file will be billed monthly until you cancel the subscription. </p>
                                        <hr>
                                        <label>Subscription Type</label>
                                        <select id="subType" class="form-control" name="subType" required>
                                            <option value="232">1 Box a Month</option>
                                            <option value="234">2 Boxes a Month</option>
                                            <option value="228">3 Boxes a Month</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <p></p>
                                    </div>
                                    <div class="col-lg-1 col-sm-1 col-xs-2">
                                        <h5 id="price">$35</h5>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        <h3>Flavors</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-sm-4 col-xs-12">
                                        <label>First</label>
                                        <select name="SubsFlavor1" class="form-control" id="flavorOne" required>
                                            <option value=""></option>
                                            <option value="All the Wei flavor assortment">All the Wei flavor assortment</option>
                                            <option value="Wei Gratitude - Chai Spices">Wei Gratitude - Chai Spices</option>
                                            <option value="Wei Love - Chili">Wei Love - Chili</option>
                                            <option value="Wei Beautiful - Peppermint">Wei Beautiful - Peppermint</option>
                                            <option value="Wei Inspired - Himalayan Pink Salt">Wei Inspired - Himalayan Pink Salt</option>
                                            <option value="Wei Joyful - Citrus">Wei Joyful - Citrus</option>
                                            <option value="Wei Pure - 70% Rich Pure Dark">Wei Pure - 70% Rich Pure Dark</option>
                                            <option value="Wei Relaxed - 68% Creamy Pure Dark">Wei Relaxed - 68% Creamy Pure Dark</option>
                                            <option value="Wei Peace Lavender Grey">Wei Peace Lavender Grey</option>
                                            <option value="Wei Radiant 80% Extra Dark">Wei Radiant 80% Extra Dark</option>
                                        </select>
                                    </div>
                                    <div id="flavor2" style="display: none;" class="col-lg-4 col-sm-4 col-xs-12">
                                        <label>Second</label>
                                        <select id="flavorTwo" name="SubsFlavor2"  class="form-control">
                                            <option value=""></option>
                                            <option value="All the Wei flavor assortment">All the Wei flavor assortment</option>
                                            <option value="Wei Gratitude - Chai Spices">Wei Gratitude - Chai Spices</option>
                                            <option value="Wei Love - Chili">Wei Love - Chili</option>
                                            <option value="Wei Beautiful - Peppermint">Wei Beautiful - Peppermint</option>
                                            <option value="Wei Inspired - Himalayan Pink Salt">Wei Inspired - Himalayan Pink Salt</option>
                                            <option value="Wei Joyful - Citrus">Wei Joyful - Citrus</option>
                                            <option value="Wei Pure - 70% Rich Pure Dark">Wei Pure - 70% Rich Pure Dark</option>
                                            <option value="Wei Relaxed - 68% Creamy Pure Dark">Wei Relaxed - 68% Creamy Pure Dark</option>
                                            <option value="Wei Peace Lavender Grey">Wei Peace Lavender Grey</option>
                                            <option value="Wei Radiant 80% Extra Dark">Wei Radiant 80% Extra Dark</option>
                                        </select>
                                    </div>
                                    <div id="flavor3" style="display: none;" class="col-lg-4 col-sm-4 col-xs-12">
                                        <label>Third</label>
                                        <select id="flavorThree" name="SubsFlavor3" class="form-control">
                                            <option value=""></option>
                                            <option value="All the Wei flavor assortment">All the Wei flavor assortment</option>
                                            <option value="Wei Gratitude - Chai Spices">Wei Gratitude - Chai Spices</option>
                                            <option value="Wei Love - Chili">Wei Love - Chili</option>
                                            <option value="Wei Beautiful - Peppermint">Wei Beautiful - Peppermint</option>
                                            <option value="Wei Inspired - Himalayan Pink Salt">Wei Inspired - Himalayan Pink Salt</option>
                                            <option value="Wei Joyful - Citrus">Wei Joyful - Citrus</option>
                                            <option value="Wei Pure - 70% Rich Pure Dark">Wei Pure - 70% Rich Pure Dark</option>
                                            <option value="Wei Relaxed - 68% Creamy Pure Dark">Wei Relaxed - 68% Creamy Pure Dark</option>
                                            <option value="Wei Peace Lavender Grey">Wei Peace Lavender Grey</option>
                                            <option value="Wei Radiant 80% Extra Dark">Wei Radiant 80% Extra Dark</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3>Your Billing Information</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input value="<?php echo isset($form_data['BillFirstName']) ? $form_data['BillFirstName']: ''; ?>" class="form-control" name="BillFirstName" type="text" placeholder="Wilhelmina" required>
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input class="form-control" value="<?php echo isset($form_data['BillLastName']) ? $form_data['BillLastName']: ''; ?>" name="BillLastName" type="text" placeholder="Wonka" required>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" value="<?php echo isset($form_data['BillAdd1']) ? $form_data['BillAdd1']: ''; ?>" name="BillAdd1" type="text" placeholder="108 Fabulous St." required>
                                </div>
                                <div class="form-group">
                                    <label>Address 2</label>
                                    <input class="form-control" value="<?php echo isset($form_data['BillAdd2']) ? $form_data['BillAdd2']: ''; ?>" name="BillAdd2" type="text" placeholder="Apt #1">
                                </div>
                                <div class="form-group">
                                    <label>City</label>
                                    <input class="form-control" value="<?php echo isset($form_data['BillCity']) ? $form_data['BillCity']: ''; ?>" name="BillCity" type="text" placeholder="Happyville" required>
                                </div>
                                <div class="form-group">
                                    <label>State</label>
                                    <select name="BillState" id="BillState" class="form-control" placeholder="State" required>
                                        <option value=""></option>
                                        <option value="AL">AL</option>
                                        <option value="AK">AK</option>
                                        <option value="AR">AR</option>
                                        <option value="AZ">AZ</option>
                                        <option value="CA">CA</option>
                                        <option value="CO">CO</option>
                                        <option value="CT">CT</option>
                                        <option value="DC">DC</option>
                                        <option value="DE">DE</option>
                                        <option value="FL">FL</option>
                                        <option value="GA">GA</option>
                                        <option value="HI">HI</option>
                                        <option value="ID">ID</option>
                                        <option value="IL">IL</option>
                                        <option value="IN">IN</option>
                                        <option value="IA">IA</option>
                                        <option value="KS">KS</option>
                                        <option value="KY">KY</option>
                                        <option value="LA">LA</option>
                                        <option value="ME">ME</option>
                                        <option value="MD">MD</option>
                                        <option value="MA">MA</option>
                                        <option value="MI">MI</option>
                                        <option value="MN">MN</option>
                                        <option value="MS">MS</option>
                                        <option value="MO">MO</option>
                                        <option value="MT">MT</option>
                                        <option value="NE">NE</option>
                                        <option value="NV">NV</option>
                                        <option value="NH">NH</option>
                                        <option value="NJ">NJ</option>
                                        <option value="NM">NM</option>
                                        <option value="NY">NY</option>
                                        <option value="NC">NC</option>
                                        <option value="ND">ND</option>
                                        <option value="OH">OH</option>
                                        <option value="OK">OK</option>
                                        <option value="OR">OR</option>
                                        <option value="PA">PA</option>
                                        <option value="RI">RI</option>
                                        <option value="SC">SC</option>
                                        <option value="SD">SD</option>
                                        <option value="TN">TN</option>
                                        <option value="TX">TX</option>
                                        <option value="UT">UT</option>
                                        <option value="VT">VT</option>
                                        <option value="VA">VA</option>
                                        <option value="WA">WA</option>
                                        <option value="WV">WV</option>
                                        <option value="WI">WI</option>
                                        <option value="WY">WY</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zip Code</label>
                                    <input class="form-control" value="<?php echo isset($form_data['BillZip']) ? $form_data['BillZip']: ''; ?>" name="BillZip" type="text" placeholder="12345" required>
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input class="form-control" value="<?php echo isset($form_data['BillPhone']) ? $form_data['BillPhone']: ''; ?>" name="BillPhone" type="text" placeholder="(555)555-5555" required>
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="form-control" value="<?php echo isset($form_data['BillEmail']) ? $form_data['BillEmail']: ''; ?>" name="BillEmail" type="text" placeholder="email@domain.com" required>
                                </div>
                                <div class="form-group">
                                    <label>How Did You Hear About Us?</label>
                                    <input class="form-control" value="<?php echo isset($form_data['Lead']) ? $form_data['Lead']: ''; ?>" name="Lead" type="text" placeholder="" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3>Gift Recipient’s Information</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input class="form-control" value="<?php echo isset($form_data['FirstName']) ? $form_data['FirstName']: ''; ?>" name="FirstName" type="text" placeholder="Wilhelmina" required>
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input class="form-control" value="<?php echo isset($form_data['LastName']) ? $form_data['LastName']: ''; ?>" name="LastName" type="text" placeholder="Wonka" required>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" value="<?php echo isset($form_data['Add1']) ? $form_data['Add1']: ''; ?>" name="Add1" type="text" placeholder="108 Fabulous St." required>
                                </div>
                                <div class="form-group">
                                    <label>Address 2</label>
                                    <input class="form-control" value="<?php echo isset($form_data['Add2']) ? $form_data['Add2']: ''; ?>" name="Add2" type="text" placeholder="Apt #1">
                                </div>
                                <div class="form-group">
                                    <label>City</label>
                                    <input class="form-control" value="<?php echo isset($form_data['City']) ? $form_data['City']: ''; ?>" name="City" type="text" placeholder="Happyville" required>
                                </div>
                                <div class="form-group">
                                    <label>State</label>
                                    <select name="State" id="State" class="form-control" placeholder="State" required>
                                        <option value=""></option>
                                        <option value="AL">AL</option>
                                        <option value="AK">AK</option>
                                        <option value="AR">AR</option>
                                        <option value="AZ">AZ</option>
                                        <option value="CA">CA</option>
                                        <option value="CO">CO</option>
                                        <option value="CT">CT</option>
                                        <option value="DC">DC</option>
                                        <option value="DE">DE</option>
                                        <option value="FL">FL</option>
                                        <option value="GA">GA</option>
                                        <option value="HI">HI</option>
                                        <option value="ID">ID</option>
                                        <option value="IL">IL</option>
                                        <option value="IN">IN</option>
                                        <option value="IA">IA</option>
                                        <option value="KS">KS</option>
                                        <option value="KY">KY</option>
                                        <option value="LA">LA</option>
                                        <option value="ME">ME</option>
                                        <option value="MD">MD</option>
                                        <option value="MA">MA</option>
                                        <option value="MI">MI</option>
                                        <option value="MN">MN</option>
                                        <option value="MS">MS</option>
                                        <option value="MO">MO</option>
                                        <option value="MT">MT</option>
                                        <option value="NE">NE</option>
                                        <option value="NV">NV</option>
                                        <option value="NH">NH</option>
                                        <option value="NJ">NJ</option>
                                        <option value="NM">NM</option>
                                        <option value="NY">NY</option>
                                        <option value="NC">NC</option>
                                        <option value="ND">ND</option>
                                        <option value="OH">OH</option>
                                        <option value="OK">OK</option>
                                        <option value="OR">OR</option>
                                        <option value="PA">PA</option>
                                        <option value="RI">RI</option>
                                        <option value="SC">SC</option>
                                        <option value="SD">SD</option>
                                        <option value="TN">TN</option>
                                        <option value="TX">TX</option>
                                        <option value="UT">UT</option>
                                        <option value="VT">VT</option>
                                        <option value="VA">VA</option>
                                        <option value="WA">WA</option>
                                        <option value="WV">WV</option>
                                        <option value="WI">WI</option>
                                        <option value="WY">WY</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zip Code</label>
                                    <input class="form-control" value="<?php echo isset($form_data['Zip']) ? $form_data['Zip']: ''; ?>" name="Zip" type="text" placeholder="12345" required>
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input class="form-control" value="<?php echo isset($form_data['Phone']) ? $form_data['Phone']: ''; ?>" name="Phone" type="text" placeholder="(555)555-5555" required>
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="form-control" value="<?php echo isset($form_data['Email']) ? $form_data['Email']: ''; ?>" id="Email" name="Email" type="text" placeholder="email@domain.com" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3>Credit Card Information</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-4 col-xs-9">
                                            <label>Credit Card Type</label>
                                            <select name="CCType" class="form-control" required>
                                                <option value="American Express">American Express</option>
                                                <option value="Discover">Discover</option>
                                                <option value="MasterCard">MasterCard</option>
                                                <option value="Visa">Visa</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-sm-4 col-xs-9" >
                                            <label>Credit Card Number</label>
                                            <input name="CCNumber" class="form-control" type="text" placeholder="1234 5678 9123 4567" required>
                                        </div>
                                        <div class="col-lg-3 col-sm-4 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                                    <label>Expiration Date</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4 col-sm-5 col-xs-5">
                                                    <select name="CCMonth" class="form-control" required>
                                                        <option value="01">01</option>
                                                        <option value="02">02</option>
                                                        <option value="03">03</option>
                                                        <option value="04">04</option>
                                                        <option value="05">05</option>
                                                        <option value="06">06</option>
                                                        <option value="07">07</option>
                                                        <option value="08">08</option>
                                                        <option value="09">09</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-5 col-sm-6 col-xs-6">
                                                    <select name="CCYear" class="form-control" required>
                                                        <?php
                                                        for($i = 0; $i < 15; $i++){
                                                            $date = date(Y) + $i;
                                                            echo '<option value="' . $date . '">' . $date . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-3 col-xs-6 form-group">
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                                    <label>Security Code/CVV</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                                    <input class="form-control" type="text" placeholder="123" name="CCSecurity" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10 col-sm-9 col-xs-7">
                    </div>
                    <div class="col-lg-2 col-sm-3 col-xs-5">
                        <button type="submit" value="submit" class="btn btn-block btn-lg placeOrder">Place Order</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>

<!-- jQuery -->
<script src="assets/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="assets/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="assets/js/sb-admin-2.js"></script>

<!-- Validator -->
<script src="assets/jquery/dist/jquery.validate.js"></script>

<!-- Validator additional methods -->
<script src="assets/jquery/dist/additional-methods.js"></script>

<script>
    $(document).ready(function(){
        // validate the comment form when it is submitted
        $("#orderForm").validate({
            rules: {
                Email: {
                    required: true,
                    email: true
                },
                BillEmail: {
                    required: true,
                    email: true
                },
                CCNumber: {
                    required: true,
                    minlength: 13,
                    maxlength: 19
                },
                Phone: {
                    required: true,
                    phoneUS: true
                },
                BillPhone: {
                    required: true,
                    phoneUS: true
                }
            }
        });

        $('#Email').change(function(){
            $.ajax({
                type: "POST",
                data: $('#Email').serialize(),
                url: "http://dev.tiredtortoise.com/samples/checkemail.php",
                success: function(data) {
                    if(data == "Hello"){
                        $('#Email').after('<div id="emailAlert" class="alert alert-danger">Oops! Your email address is already connected to an account with us, and in order to make this work smoothly, we need to link this to a separate email. If you have another email you can use, please enter it now. If not, call us at 602-577-2338 and we’ll help you out. </div>');
                    }else{
                        $('#emailAlert').hide();
                    }

                }
            });
        });

        //make changes to flavours on page load
        $("#subType").ready(function(){
            if($("#subType").val() == "232"){
                $("#flavor2").hide();
                $("#flavor3").hide();
                $("#price").text('$35');
                $("#pic2").hide();
                $("#pic3").hide();
                $("#pic1").show();
                $( "#flavorTwo" ).rules( "remove" );
                $( "#flavorThree" ).rules( "remove" );
            }else if($("#subType").val() == "234"){
                $("#flavor2").show();
                $("#flavor3").hide();
                $("#price").text('$59');
                $("#pic1").hide();
                $("#pic3").hide();
                $("#pic2").show();
                $( "#flavorTwo" ).rules( "add", {
                    required: true
                });
                $( "#flavorThree" ).rules( "remove" );
            }else if($("#subType").val() == "228"){
                $("#flavor2").show();
                $("#flavor3").show();
                $("#price").text('$89');
                $("#pic2").hide();
                $("#pic1").hide();
                $("#pic3").show();
                $( "#flavorTwo" ).rules( "add", {
                    required: true
                });
                $( "#flavorThree" ).rules( "add", {
                    required: true
                });
            }
        });



        $("#subType option[value='<?php echo isset($form_data['subType']) ? $form_data['subType']: ''; ?>']").attr("selected", "selected");
        $("#flavorOne option[value='<?php echo isset($form_data['SubsFlavor1']) ? $form_data['SubsFlavor1']: ''; ?>']").attr("selected", "selected");
        $("#flavorTwo option[value='<?php echo isset($form_data['SubsFlavor2']) ? $form_data['SubsFlavor2']: ''; ?>']").attr("selected", "selected");
        $("#flavorThree option[value='<?php echo isset($form_data['SubsFlavor3']) ? $form_data['SubsFlavor3']: ''; ?>']").attr("selected", "selected");
        $("#BillState option[value='<?php echo isset($form_data['BillState']) ? $form_data['BillState']: ''; ?>']").attr("selected", "selected");
        $("#State option[value='<?php echo isset($form_data['State']) ? $form_data['State']: ''; ?>']").attr("selected", "selected");


        $("#subType").change(function(){
            if($("#subType").val() == "232"){
                $("#flavor2").hide();
                $("#flavor3").hide();
                $("#price").text('$35');
                $("#pic2").hide();
                $("#pic3").hide();
                $("#pic1").show();
                $( "#flavorTwo" ).rules( "remove" );
                $( "#flavorThree" ).rules( "remove" );
            }else if($("#subType").val() == "234"){
                $("#flavor2").show();
                $("#flavor3").hide();
                $("#price").text('$59');
                $("#pic1").hide();
                $("#pic3").hide();
                $("#pic2").show();
                $( "#flavorTwo" ).rules( "add", {
                    required: true
                });
                $( "#flavorThree" ).rules( "remove" );
            }else if($("#subType").val() == "228"){
                $("#flavor2").show();
                $("#flavor3").show();
                $("#price").text('$89');
                $("#pic2").hide();
                $("#pic1").hide();
                $("#pic3").show();
                $( "#flavorTwo" ).rules( "add", {
                    required: true
                });
                $( "#flavorThree" ).rules( "add", {
                    required: true
                });
            }
        });
    });
</script>

</html>